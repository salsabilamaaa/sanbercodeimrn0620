//Soal 1
function range(startNum, finishNum){
	var arrayAngka = []
	
	if(!startNum || !finishNum){
		arrayAngka.push(-1)
	}else{
		arrayAngka = []
		if (startNum < finishNum){
			for(i = startNum; i <= finishNum; i++){
				arrayAngka.push(i)
			}
		}else{
			for(i = startNum; i >= finishNum; i--){
				arrayAngka.push(i)
			}
		}
	}
	return arrayAngka
}

console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range(), "\n")

//Soal 2
function rangeWithStep(startNum, finishNum, step){
	var arrayAngka = []
	
	if(!startNum || !finishNum){
		arrayAngka.push(-1)
	}else{
		if (startNum < finishNum){
			for(i = startNum; i <= finishNum; i = i+step){
				arrayAngka.push(i)
			}
		}else{
			for(i = startNum; i >= finishNum; i = i-step){
				arrayAngka.push(i)
			}
		}
	}
	return arrayAngka
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4), "\n")

//Soal 3
function sum(awal, akhir, step){
	var arrayAngka = []
	var jum = 0

	if(!awal){
		return 0
	}else if(!akhir){
		arrayAngka.push(awal)
	}else if(!step){
		arrayAngka = range(awal, akhir)
	}else{
		arrayAngka = rangeWithStep(awal, akhir, step)
	}

	for(i = 0; i < arrayAngka.length; i++){
		jum += arrayAngka[i]
	}
	return jum
}

console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum(), "\n")

//Soal 4
function dataHandling(arr){
	for(i = 0; i < arr.length; i++){
		console.log("Nomor ID 	:", arr[i][0])
		console.log("Nama 		:", arr[i][1])
		console.log("TTL		:", arr[i][2], arr[i][3])
		console.log("Hobby		:", arr[i][4], "\n")
	}
}
var input = [ ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
              ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
              ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
              ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]]

console.log(dataHandling(input))

//Soal 5
function balikKata(word){
	var balik = []
	
	for(i=0; i < word.length; i++){
		balik.unshift(word[i])
	}

	var kata = balik.join("")
	return kata
}

console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"), "\n")

//Soal 6
function dataHandling2(arr){
	arr.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung")
	arr.splice(4, 0, "Pria", "SMA International Metro")
	console.log(arr, "\n")

	var TTL = arr[3].split("/")
	switch(TTL[1]) {
		case "01" : { 
			console.log("Januari")
			break
		}
		case "02" : {
			console.log("Februari")
			break
		}
		case "03" : {
			console.log("Maret")
			break
		}
		case "04" : {
			console.log("April")
			break
		}
		case "05" : {
			console.log("Mei")
			break
		}
		case "06" : {
			console.log("Juni")
			break
		}
		case "07" : {
			console.log("Juli")
			break
		}
		case "08" : {
			console.log("Agustus")
			break
		}
		case "09" : {
			console.log("September")
			break
		}
		case "10" : {
			console.log("Oktober")
			break
		}
		case "11" : {
			console.log("November")
			break
		}
		case "12" : {
			console.log("Desember")
			break
		}
		default : {
			console.log("Tidak ada bulan", TTL[1])
		}
	}

	var sortTTL = TTL
	
	console.log(sortTTL.sort(), "\n")	

	console.log(TTL.join(" - "), "\n")

	console.log(arr[1].slice(0,14))
}

var output = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]

console.log(dataHandling2(output))