//Soal 1
var i = 0

while(i < 20){
	i += 2
	console.log(i, "- I Love Coding")
}
console.log("")

while(i > 0){
	console.log(i, "- I will become a mobile developer")
	i -= 2
}
console.log("")

//Soal 2
var i

for(i = 1; i <= 20; i++){
	if(i%2 != 0){
		if(i%3 == 0){
			console.log(i + " - I Love Coding")
		}else{
			console.log(i + " - Santai")
		}
	}else{
		console.log(i + " - Berkualitas")
	}
}
console.log("")

//Soal 3
var j
var word = "#"

for(j = 0; j < 4; j++){
	var pagar = ""
	var i
	for(i = 0; i < 8; i++){
		pagar = pagar.concat(word)
	}
	console.log(pagar)	
}
console.log("")

//Soal 4
var i 
var word = "#"
var pagar = ""
for(i = 0; i < 7; i++){
	pagar = pagar.concat(word)
	console.log(pagar)
}
console.log("")

//Soal 5
var i, j
var catur = " "
var line
var word = "#"
for(i = 0; i <8; i++){
	line = ""
	if (i%2 != 0){
		j = 0;
		for(j = 0; j < 8; j++){
			if(j%2 != 0){
				line = line.concat(catur)
			}else{
				line = line.concat(word)
			}
		}
	}else{
		j = 0;
		for(j = 0; j < 8; j++){
			if(j%2 != 0){
				line = line.concat(word)
			}else{
				line = line.concat(catur)
			}
		}
	}
	console.log(line)
}