//A. Balik String
function balikString(word){
	var kata = ""
	var i = word.length - 1
	while(i >= 0){
		kata = kata.concat(word[i])
		i--
	}
	return kata
}

//B. Palindrome 
function palindrome(word){
	var kata = ""
	var i = word.length - 1
	while(i >= 0){
		kata = kata.concat(word[i])
		i--
	}
	return kata == word
}

//C. Bandingkan Angka
function bandingkan(angka1 = 0, angka2 = 0){
	if(angka1 < 0 || angka2 < 0){
		return -1
	}else{
		if(angka1 > angka2){
			return angka1
		}else if(angka2 > angka1){
			return angka2
		}else{
			return -1
		}
	}
}

//TEST CASES balikString
console.log(balikString("abcde"))
console.log(balikString("rusak"))
console.log(balikString("racecar"))
console.log(balikString("haji"), "\n")

//TEST CASES Palindrome
console.log(palindrome("kasur rusak"))
console.log(palindrome("haji ijah"))
console.log(palindrome("nabasan"))
console.log(palindrome("nababan"))
console.log(palindrome("jakarta"), "\n")

//TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15))
console.log(bandingkan(12, 12))
console.log(bandingkan(-1, 10))
console.log(bandingkan(112, 121))
console.log(bandingkan(1))
console.log(bandingkan())
console.log(bandingkan("15", "18"))