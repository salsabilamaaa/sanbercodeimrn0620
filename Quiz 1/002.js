//A. Ascending Ten
function AscendingTen(Number){
	var angka = ""
	if(!Number){
		return -1
	}else{
		for(i = 0; i < 10; i++){
			angka = angka.concat(Number, " ")
			Number++
		}
		return angka
	}
}

//B. Descending Ten 
function DescendingTen(Number){
	var angka = ""
	if(!Number){
		return -1
	}else{
		for(i = 10; i > 0; i--){
			angka = angka.concat(Number, " ")
			Number--
		}
		return angka
	}
}

//C. Conditional Ascending Descending
function ConditionalAscDesc(reference, check){
	if(!reference || !check){
		return -1
	}else{
		if(check%2 == 0){
			return DescendingTen(reference)
		}else{
			return AscendingTen(reference)
		}
	}
}

//D. Papan Ular Tangga
function ulartangga(){
	var angka = 100
	var i = 9
	var ular = ConditionalAscDesc(angka, 10)
	while(i > 0){
		if(i%2 != 0){
			angka -= 19
		}else{
			angka -= 1
		}
		ular = ular.concat("\n", ConditionalAscDesc(angka, i))
		i--
	}
	return ular
}

//TEST CASES Ascending Ten
console.log(AscendingTen(11))
console.log(AscendingTen(21))
console.log(AscendingTen(), "\n")

//TEST CASES Descending Ten
console.log(DescendingTen(100))
console.log(DescendingTen(10))
console.log(DescendingTen(), "\n")

//TEST CASES Conditional Ascending Descending
console.log(ConditionalAscDesc(20, 8))
console.log(ConditionalAscDesc(81, 1))
console.log(ConditionalAscDesc(31))
console.log(ConditionalAscDesc(), "\n")

//TEST CASE Ular Tangga
console.log(ulartangga())