//soal if-else
var nama = "John"
var peran = ""

if (nama == ""){
	console.log("Nama harus diisi!")
}else if (nama != "" && peran == ""){
	console.log("Halo", nama, "Pilih peranmu untuk memulai game")
}else if (nama != "" && peran != ""){
	console.log("Selamat datang di Dunia Werewolf " + nama)
	if (peran == 'Guard'){
		console.log("Halo " + peran + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
	}else if (peran == 'Penyihir'){
		console.log("Halo " + peran + nama + ", kamu dapat melihat siapa yang menjadi werewolf")
	}else if (peran == 'Guard'){
		console.log("Halo " + peran + nama + ", kamu akan memakan mangsa setiap malam!")
	}else{
		console.log("Halo " + nama + " tidak ada peran " + peran)
	}
}
console.log("")
//soal switch-case
var tanggal = 16
var bulan = 6
var tahun = 2020

switch(bulan){
	case 1 : {
		if (tanggal > 0 && tanggal <= 31){
			console.log(tanggal + " Januari " + tahun)
		}else{
			console.log("Tidak ada tanggal " + tanggal + " di bulan Januari")
		}
		break;
	}
	case 2 : {
		if (tahun%4 != 0){
			if (tanggal > 0 && tanggal < 29){
				console.log(tanggal + " Februari " + tahun)
			}else{
				console.log("Tidak ada tanggal " + tanggal + " di bulan Februari")
			}
		}else{
			if (tanggal > 0 && tanggal < 30){
				console.log(tanggal + " Februari " + tahun)
			}else{
				console.log("Tidak ada tanggal " + tanggal + " di bulan Februari")
			}
		}
		break;
	}
	case 3 : {
		if (tanggal > 0 && tanggal <= 31){
			console.log(tanggal + " Maret " + tahun)
		}else{
			console.log("Tidak ada tanggal " + tanggal + " di bulan Maret")
		}
		break;
	}
	case 4 : {
		if (tanggal > 0 && tanggal <= 30){
			console.log(tanggal + " April " + tahun)
		}else{
			console.log("Tidak ada tanggal " + tanggal + " di bulan April")
		}
		break;
	}
	case 5 : {
		if (tanggal > 0 && tanggal <= 31){
			console.log(tanggal + " Mei " + tahun)
		}else{
			console.log("Tidak ada tanggal " + tanggal + " di bulan Mei")
		}
		break;
	}
	case 6 : {
		if (tanggal > 0 && tanggal <= 30){
			console.log(tanggal + " Juni " + tahun)
		}else{
			console.log("Tidak ada tanggal " + tanggal + " di bulan Juni")
		}
		break;
	}
	case 7 : {
		if (tanggal > 0 && tanggal <= 31){
			console.log(tanggal + " Juli " + tahun)
		}else{
			console.log("Tidak ada tanggal " + tanggal + " di bulan Juli")
		}
		break;
	}
	case 8 : {
		if (tanggal > 0 && tanggal <= 31){
			console.log(tanggal + " Agustus " + tahun)
		}else{
			console.log("Tidak ada tanggal " + tanggal + " di bulan Agustus")
		}
		break;
	}
	case 9 : {
		if (tanggal > 0 && tanggal <= 30){
			console.log(tanggal + " September " + tahun)
		}else{
			console.log("Tidak ada tanggal " + tanggal + " di bulan September")
		}
		break;
	}
	case 10 : {
		if (tanggal > 0 && tanggal <= 31){
			console.log(tanggal + " Oktober " + tahun)
		}else{
			console.log("Tidak ada tanggal " + tanggal + " di bulan Oktober")
		}
		break;
	}
	case 11 : {
		if (tanggal > 0 && tanggal <= 30){
			console.log(tanggal + " November " + tahun)
		}else{
			console.log("Tidak ada tanggal " + tanggal + " di bulan November")
		}
		break;
	}
	case 12 : {
		if (tanggal > 0 && tanggal <= 31){
			console.log(tanggal + " Desember " + tahun)
		}else{
			console.log("Tidak ada tanggal " + tanggal + " di bulan Desember")
		}
		break;
	}
	default : { console.log("Tidak ada bulan ke " + bulan)}
}